/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simonwoodworth.javamavencalcinclassdemo;

/**
 *
 * @author simon
 */
public class Calculator {
    
    public int add (int a, int b) {
        return a + b;
    }
    
    public int subtract (int a, int b) {
        return a - b;
    }
    
    public int multiply (int a, int b) {
        return a * b;
    }
    
    public int divide (int a, int b) {
        return a / b;
    }

    public int square(int a) {
        return a * a;
    }

    public int cube(int a) {
        return a * a * a;
    }

    public int modulo(int a, int b) {
        return a % b;
    }
    // Add a c to the return statement- with same function as in a and b 
    public int addThree (int a, int b, int c) {
        return a + b + c;
    }
    // Add a c to the return statement- with same function as in a and b 
    public int subtractThree (int a, int b, int c) {
        return a - b - c;
    }
    // Add a c to the return statement- with same function as in a and b 
    public int multiplyThree (int a, int b, int c) {
        return a * b * c;
    }
    // Add a c to the return statement- with same function as in a and b 
    public int divideThree (int a, int b, int c) {
        return a / b / c;
    }
    
}
